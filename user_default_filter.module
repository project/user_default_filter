<?php

/**
 * @file
 *
 * Allow the user to choose a default filter from those allowed by their role(s)
 */

/**
 * Implementation of hook_user
 *
 * allow the user to choose which default filter they want from those they have
 * access to
 */
function user_default_filter_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {
    case 'form':
      $form = array();
      $form['user_default_filter'] = array(
        '#type' => 'fieldset',
        '#title' => t('Input Filters'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $form['user_default_filter']['user_default_filter_choice'] = array(
        '#title' => t('Default Input Filter'),
        '#description' => t('Which input filter do you want to be selected by default?'),
        '#type' => 'radios',
        '#default_value' => variable_get('user_default_filter_choice-'. $account->uid, 0),
        '#options' => user_default_filter_user_get_filter_formats($account),
      );
      return $form;
      break;
    case 'update':
      //drupal_set_message('$edit = <pre>'. print_r($edit, TRUE) .'</pre>');
      $account->user_default_filter = $edit['user_default_filter_choice'];
      variable_set('user_default_filter_choice-'. $account->uid, $edit['user_default_filter_choice']);
      unset($edit['user_default_filter_choice']);
      break;
  }
}

/**
 * Given the user account return an array of all format filters that they have
 * access to
 */
function user_default_filter_user_get_filter_formats($account) {
  $opts = filter_formats();
  // the filter_formats function function will only return the
  $options = array();
  foreach ($opts AS $row) {
    $options[$row->format] = $row->name;
  }
  return $options;
}

/**
 * Implementation of hook_form_alter().
 *
 * Alter all node/block forms, any form that would have an input filter option,
 * and set the option to the user's selected default
 */
function user_default_filter_form_alter(&$form, $form_state, $form_id) {
  // check to see if this form needs skipped because we are editing, not
  // creating a new item
  if (
    ($form['#id'] == 'node-form' && !empty($form['nid']['#value'])) ||  // node edit form
    ($form_id == 'block_admin_configure') ||                            // the new blocks ->  $form_id == 'block_add_block_form'
    ($form_id == 'comment_form' && !empty($form['cid']['#value']))      // the comment edit form
  ) {
    return NULL;
  }
  $form = user_default_filter_form_item_alter($form);
}

/**
 * recursively traverse the $form and when we wild a filter_form element we set
 * it to the user's default if there is one set
 */
function user_default_filter_form_item_alter($form) {
  foreach ($form AS $key => $value) {
    if (is_array($value) && isset($value['#element_validate']) && ($value['#element_validate'] == array('filter_form_validate')) && ($value['#type'] == 'fieldset')) {
      // this is the filter_form fieldset
      global $user;
      $user_default_filter = variable_get('user_default_filter_choice-'. $user->uid, 0);
      foreach ($value AS $sub_key => $sub_value) {
        if(is_int($sub_key)) {
          $form[$key][$sub_key]['#default_value'] = $user_default_filter ? $user_default_filter : $form[$key][$sub_key]['#default_value'];
        }
      }
    }
    else if (is_array($value)) {
      $form[$key] = user_default_filter_form_item_alter($value);
    } 
  }
  return $form;
}

